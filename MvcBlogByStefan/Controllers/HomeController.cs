﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcBlogByStefan.Models;

namespace MvcBlogByStefan.Controllers
{
    public class HomeController : Controller
    {
        ContentRepository contentRepository = new ContentRepository();

        // GET: /Home/
        public ActionResult Index(int? page)
        {
            const int pageSize = 10;

            var tempPosts = contentRepository.FindPosts();
            var paginatedPosts = tempPosts.Skip((page ?? 0) * pageSize).Take(pageSize).ToList();

            return View(paginatedPosts);
        }

        // GET: /Home/Users/Stefan
        public ActionResult Users(string id)
        {
            var tempPosts = contentRepository.FindPostsByUser(id);
            var paginatedUserPosts = tempPosts.ToList();
            ViewBag.User = id;

            return View(paginatedUserPosts);
        }

        // GET: /Home/Content/2
        public ActionResult Content(int id)
        {
            Post myPost = contentRepository.GetPost(id);

            var tempComments = contentRepository.FindCommentsForPost(id);
            var allCommentsForPost = tempComments.ToList();
            ViewData["Comments"] = allCommentsForPost;

            if (myPost == null)
                return View("NotFound");
            else
                return View(myPost);
        }

        // POST: /Home/Content/2
        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult Content(int id, FormCollection formValues)
        {
            Comment comment = new Comment()
            {
                parent_post_id = Convert.ToInt32(Request.Form["post_id"]),
                comment_creator = User.Identity.Name,
                comment_created_on = DateTime.Now
            };

            if (Request.Form["comment_text"] == "")
            {
                return RedirectToAction("Content", new { id = comment.parent_post_id });
            }
            else
            {
                comment.comment_text = Request.Form["comment_text"];
            }

            contentRepository.AddComment(comment);
            contentRepository.Save();
            return RedirectToAction("Content", new { id = comment.parent_post_id });
        }

        // GET: /Home/Edit/2
        [Authorize]
        public ActionResult Edit(int id)
        {
            Post post = contentRepository.GetPost(id);
            if (post.post_creator != User.Identity.Name)
            {
                return View("InvalidOwner");
            }
            return View(post);
        }

        // POST: /Home/Edit/2
        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult Edit(int id, FormCollection formValues)
        {
            // Retrieve existing post
            Post post = contentRepository.GetPost(id);
            if (post.post_creator != User.Identity.Name)
            {
                return View("InvalidOwner");
            }

            // Update post with form posted values
            
            if (Request.Form["post_text"].Length < 200)
            {
                post.post_excerpt = Request.Form["post_text"] + "...";
            }
            else
            {
                post.post_excerpt = Request.Form["post_text"].Substring(0, 200) + "...";
            }

            if (Request.Form["post_text"] == "" || Request.Form["post_title"] == "")
            {
                return RedirectToAction("Edit", new { id = post.post_id });
            }
            else
            {
                post.post_text = Request.Form["post_text"];
                post.post_title = Request.Form["post_title"];
            }

            post.post_text = Request.Form["post_text"];
            post.post_edited_on = DateTime.Now;
            // Persist changes back to database
            contentRepository.Save();
            // Perform HTTP redirect to details page for the saved Post
            return RedirectToAction("Content", new { id = post.post_id });
        }

        // GET: /Home/Create
        [Authorize]
        public ActionResult Create()
        {
            Post post = new Post();
            return View(post);
        }

        // POST: /Home/Create
        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult Create(FormCollection formValues)
        {
            Post post = new Post()
            {
                post_title = Request.Form["post_title"],
                post_text = Request.Form["post_text"],
                post_creator = User.Identity.Name,
                post_created_on = DateTime.Now
            };

            if (Request.Form["post_text"].Length < 200)
            {
                post.post_excerpt = Request.Form["post_text"] + "...";
            }
            else
            {
                post.post_excerpt = Request.Form["post_text"].Substring(0, 200) + "...";
            }

            contentRepository.AddPost(post);
            contentRepository.Save();
            return RedirectToAction("Content", new { id = post.post_id });
        }

        // GET: /Posts/Delete/1
        [Authorize]
        public ActionResult Delete(int id)
        {
            Post post = contentRepository.GetPost(id);

            if (post != null && post.post_creator != User.Identity.Name)
            {
                return View("InvalidOwner");
            }

            if (post == null)
                return View("NotFound");
            else
                return View(post);
        }

        // POST: /Posts/Delete/1
        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult Delete(int id, string confirmButton)
        {
            Post post = contentRepository.GetPost(id);

            if (post != null && post.post_creator != User.Identity.Name)
            {
                return View("InvalidOwner");
            }

            if (post == null)
                return View("NotFound");

            contentRepository.DeletePost(post);
            contentRepository.Save();
            return View("Deleted");
        }

        // GET: /Home/About
        public ActionResult About()
        {
            return View();
        }
    }
}
