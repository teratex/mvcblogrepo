﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcBlogByStefan.Models
{
    public class ContentRepository
    {
        private MvcBlogByStefanDataContext db = new MvcBlogByStefanDataContext();

        // Query Methods
        public IQueryable<Post> FindPosts()
        {
            return from post in db.Posts
                   orderby post.post_created_on descending
                   select post;
        }
        public IQueryable<Post> FindPostsByUser(string id)
        {
            return from post in db.Posts
                   where post.post_creator == id
                   orderby post.post_created_on descending
                   select post;
        }
        public Post GetPost(int id)
        {
            return db.Posts.SingleOrDefault(d => d.post_id == id);
        }

        public IQueryable<Comment> FindCommentsForPost(int id)
        {
            return from comment in db.Comments
                   where comment.parent_post_id == id
                   orderby comment.comment_created_on descending
                   select comment;
        }

        //
        // Insert/Delete Methods
        public void AddPost(Post post)
        {
            db.Posts.InsertOnSubmit(post);
        }
        public void AddComment(Comment comment)
        {
            db.Comments.InsertOnSubmit(comment);
        }

        public void DeletePost(Post post)
        {
            db.Comments.DeleteAllOnSubmit(post.Comments);
            db.Posts.DeleteOnSubmit(post);
        }

        // Persistence
        public void Save()
        {
            db.SubmitChanges();
        }
    }
}