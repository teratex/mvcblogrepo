﻿function contentPrep() {
    $("#fieldset_contents").css("display", "none");

    $("#legend_text").click(function () {
        $("#fieldset_contents").toggle("slow");
    });

    $("#comment_form").submit(function () {
        if ($("#comment_text").val() == "") {
            $("#content_error").html("The comment field cannot be empty.");
            $("#content_error").css("color", "red");
            return false;
        }
        else {
            return true;
        }
    });
}

function editPrep() {
    $("#edit_form").submit(function () {
        if ($("#post_text").val() == "" || $("#post_title").val() == "") {
            $("#content_error").html("None of the fields can be left empty");
            $("#content_error").css("color", "red");
            return false;
        }
        else {
            return true;
        }
    });
}

function createPrep() {
    $("#create_form").submit(function () {
        if ($("#post_text").val() == "" || $("#post_title").val() == "") {
            $("#content_error").html("None of the fields can be left empty");
            $("#content_error").css("color", "red");
            return false;
        }
        else {
            return true;
        }
    });
}

$(document).ready(function () {
    var location = window.location.pathname;
    var locationArray = location.split("/");

    // Index
    if (location == "/" || location == "/Home" || location == "/Home/") {
    }

    // Content
    else if (locationArray[2] == "Content") {
        contentPrep();
    }

    // Edit
    else if (locationArray[2] == "Edit") {
        editPrep();
    }

    // Create
    else if (locationArray[2] == "Create") {
        createPrep();
    }

    // LogOn
    else if (locationArray[2] == "LogOn") {
    }

    // Register
    else if (locationArray[2] == "Register") {
    }

    // About
    else if (locationArray[2] == "About") {
    }
});